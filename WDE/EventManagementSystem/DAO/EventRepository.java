package de.fim.unipassau.WDE.EventManagementSystem.DAO;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import de.fim.unipassau.WDE.EventManagementSystem.Model.Event;

/**
 * The Repository for querying the Event-Database contains provides custom methods in addition to 
 * the automatically implemented Spring functionality.
 * @author Jonas Picker
 *
 */
@Repository
public interface EventRepository extends CrudRepository<Event, String> {
	//ok
	/**
	 * For the Event Database to be filtered after specific Event locations.
	 * 
	 * @param substring the part of a location or position that is queried for.
	 * @return a List of Events matching the specified search parameters.
	 */
	@Query(value = "SELECT * FROM Event e WHERE e.location LIKE '%?1%'", nativeQuery = true)
	List<Event> findEventsByLocationSubstring(String substring);
	
	/**
	 * For the Event Database to be filtered after part of its name.
	 * 
	 * @param substring the phrase to be searched.
	 * @return a List of Events matching the specified search parameters.
	 */
	@Query(value = "SELECT * FROM Event e WHERE e.name LIKE '%?'", nativeQuery = true)
	List<Event> findEventsByNameSubstring(String substring);
	
	/**
	 * This is used by the REST end-point to send out the most recent 20 events.
	 * 
	 * @param date usually the time of the request but in theory any time.
	 * @return the result set ordered by date.
	 */
	List<Event> findTop20ByOrderByDateDesc(LocalDateTime date);
	
	/**
	 * This is used to provide the initial list of events to the index-page. The maximum length of this list is encoded here.
	 * 
	 * @param date usually the time of the request but in theory any time.
	 * @return the result set ordered by date.
	 */
	List<Event> findTop42ByOrderByDateDesc(LocalDateTime date);
}

package de.fim.unipassau.WDE.EventManagementSystem.Model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * The database representation of an Event.
 * @author Jonas Picker
 *
 */
@Entity
public class Event {
		
	/**
	 * The name given to an event by the creator. The name is unique across all saved events and is converted to the primary key.
	 */
	@Id
	private String name;
		
	/**
	 * The place where the event takes place is either described by geographical position or location name.
	 */
	private String location;
	
	/**
	 * The date and starting time of the event.
	 */
	private LocalDateTime date;
	
	/**
	 * The predefined category this event fits into.
	 */
	private String type;
	
	/**
	 * The creators description provided for this event. 
	 */
	private String description;
	
	/**
	 * The number of visitors of the site that marked this event to be interesting.
	 */
	private int likes;
	
	/**
	 * The number of visitors of the site that were'nt interested in this event.
	 */
	private int dislikes;
	
	/**
	 * Initialization of a new Event instance with all its fields except likes and dislikes.
	 * 
	 * @param name
	 * @param location
	 * @param date
	 * @param type
	 * @param description
	 */
	public Event(String name, Location location, LocalDateTime date, String type, String description) {
		this.likes = 0;
		this.dislikes = 0;
		this.date = date;
		this.type = type;
		this.description = description;
		if (location.getAddressOrName() == null) {
			this.location = location.getLatt_long();
		} else {
			this.location = location.getAddressOrName();
		}
	}
	
	/**
	 * Initialization of an Event instance with all its fields set.
	 * 
	 * @param name
	 * @param location
	 * @param date
	 * @param type
	 * @param description
	 * @param likes
	 * @param dislikes
	 */
	public Event(String name, Location location, LocalDateTime date, String type, String description, int likes, int dislikes) {
		this.likes = 0;
		this.dislikes = 0;
		this.date = date;
		this.type = type;
		this.description = description;
		if (location.getAddressOrName() == null) {
			this.location = location.getLatt_long();
		} else {
			this.location = location.getAddressOrName();
		}
		this.likes = likes;
		this.dislikes = dislikes;
	}
	
	/**
	 * A placeholder Event which contains no values can also be initialized.
	 * 
	 * @param whatever A boolean to differentiate from the standard constructor
	 */
	public Event(boolean whatever) {
		this.likes = 0;
		this.dislikes = 0;
		this.date = null;
		this.type = null;
		this.description = null;
		this.location = null;
	}

	//The fields need to bee accessed from other parts of the application, so getters/setters are provided below.
	public String getName() {
		return name;
	}

	public String getLocation() {
		return location;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public String getType() {
		return type;
	}

	public String getDescription() {
		return description;
	}

	public int getLikes() {
		return likes;
	}

	public int getDislikes() {
		return dislikes;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public void setDislikes(int dislikes) {
		this.dislikes = dislikes;
	}
	
	
	
}

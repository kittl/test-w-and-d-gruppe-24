package de.fim.unipassau.WDE.EventManagementSystem.Model;

import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * This table hold a location on the globe in the database, either by name or geographical position.
 * @author Jonas Picker
 *
 */
public class Location {
		
	/**
	 * A textual representation of lattitude and longitude used by the https://www.metaweather.com/api.
	 */
	private String latt_long;
	
	/**
	 * A String that refers to a place on the globe by address or location name.
	 */
	private String addressOrName;
	
	private boolean isGeoPos;
	
	/**
	 * Initializes a location with its geographical position.
	 * 
	 * @param longitude
	 * @param lattitude
	 */
	public Location (float latt, float longi) {
		this.addressOrName = null;
		DecimalFormat df = new DecimalFormat("0.00");
		df.setRoundingMode(RoundingMode.HALF_EVEN);
		this.latt_long = df.format(latt).toString() + "," + df.format(longi).toString();
		this.isGeoPos = true;
	}
	
	/**
	 * Initializes a location with address or its name.
	 * 
	 * @param adressOrName
	 */
	public Location (String adressOrName) {
		this.latt_long = null;
		this.addressOrName = adressOrName;
		this.isGeoPos = false;
	}

	/**
	 * A getter for the geographical position.
	 * 
	 * @return the position in the short format.
	 */
	public String getLatt_long() {
		return latt_long;
	}

	/**
	 * A getter for the location name or address.
	 * 
	 * @return the String representing the above.
	 */
	public String getAddressOrName() {
		return addressOrName;
	}	
	
	
}

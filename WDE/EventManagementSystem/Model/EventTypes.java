package de.fim.unipassau.WDE.EventManagementSystem.Model;

import java.util.List;
/**
 * This capsules a predefined and/or modifiable set of categories for Events.
 * @author Jonas Picker
 *
 */
public class EventTypes { //wip
	
	private List<String> CustomTypes;
	
	public EventTypes () {
		
	}
	
	public boolean addType(String type) {
		if (CustomTypes.contains(type)) {
			return false;
		} else {
			CustomTypes.add(type);
			return true;
		}
	}
	
	public boolean removeType(String type) {
		return CustomTypes.remove(type);
	}
	
}

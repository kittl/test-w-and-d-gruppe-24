package de.fim.unipassau.WDE.EventManagementSystem.Controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import de.fim.unipassau.WDE.EventManagementSystem.DAO.EventRepository;
import de.fim.unipassau.WDE.EventManagementSystem.Model.Event;
import de.fim.unipassau.WDE.EventManagementSystem.Services.EventServiceImpl;

@Controller
public class WebAppController {
		
	@Autowired
	private EventRepository eventRepository;
	
	@Autowired
	private EventServiceImpl eventService;
	
	@RequestMapping(value =  {"/", "/index.html"}, method = RequestMethod.GET) 
	public String index(Model model) {
		
		LocalDateTime now = LocalDateTime.now();
		List<Event> recentEvents = eventRepository.findTop42ByOrderByDateDesc(now);
		
		model.addAttribute("recentEvents", recentEvents);
		
		return "index";
	}
	
	@RequestMapping(value = "/search/name", method = RequestMethod.GET)                    // -> 2 verschiedene suchfelder plox
	public String searchEventsByName (@RequestParam(value = "name", required = true) String nameSubstring, Model model) {
		
		List<Event> subStringMatches = eventRepository.findEventsByNameSubstring(nameSubstring);
		
		model.addAttribute("resultList", subStringMatches);
		
		return "index";
	}
	
	@RequestMapping(value = "/search/location", method = RequestMethod.GET)
	public String searchEventsByLocation(@RequestParam(value = "location", required = true) String locationsubString, Model model) {
		
		List<Event> subStringMatches = eventRepository.findEventsByLocationSubstring(locationsubString);
		
		model.addAttribute("resultList", subStringMatches);
		
		return "index";
	}
	
	@RequestMapping(value = "/event", method = RequestMethod.GET)                          //parameterübertragung via JSON, nicht gefunden => platzhalterevent
	public String displayDetailedView(@RequestParam(value = "name", required = true) String eventId, Model model) {
		
		Optional<Event> event = eventRepository.findById(eventId);
		
		//OrElse should never be accessed since name is primary key
		this.eventService = new EventServiceImpl(event.orElse(new Event(false)));
		String json = eventService.toJson();
		
		model.addAttribute("event", json);
		
		return "detailedView"; 		
	}
	// OK
	@RequestMapping(value = "/submit", method = RequestMethod.POST)                  //JSON in mit den Felder/Value Paaren der Eventklasse mit zusätzlichem																					
	public void submitNewEvent(@RequestBody String json) {                           //boolean Feld isGeoPos nach "location" -> 2 submitfelder (1x coord, 1x adresse/name)
		Event newEvent = eventService.fromJson(json);
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.GET)                   //Man kann nicht liken und disliken gleichzeitig -> 1 parameter reicht
	public String updateEvent(@RequestParam(value = "name", required = true) String name, @RequestParam(value = "diff", required = true) int diff, Model model) {
		Optional<Event> existingEvent = eventRepository.findById(name);
		
		//OrElse should never be accessed since name is primary key
		this.eventService = new EventServiceImpl(existingEvent.orElse(new Event(false)));
		
		if (diff < 0) {
			eventService.updateLikesAndDislikes(0, diff);
		} else if (diff > 0) {
			eventService.updateLikesAndDislikes(diff, 0);
		}
		
		model.addAttribute("event", eventService.toJson());
		
		return "detailedView";
	}
	

}

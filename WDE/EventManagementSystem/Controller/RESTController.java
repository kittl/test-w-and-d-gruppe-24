package de.fim.unipassau.WDE.EventManagementSystem.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import de.fim.unipassau.WDE.EventManagementSystem.Services.RESTServiceImpl;

/**
 * This Controller maps HTTP requests meant for the REST end-point of the application to the corresponding Service.
 * @author Jonas Picker
 *
 */
@RestController
@RequestMapping(value = "/events?n=20", method = RequestMethod.GET)
public class RESTController {
	
	/**
	 * The corresponding Service.
	 */
	@Autowired
	private RESTServiceImpl restService;
	
	/**
	 * Automatically fills the response body that belongs to the incoming request.
	 * 
	 * @return A String in JSON format is injected into the response.
	 */
	@RequestMapping(produces = "application/json")
	public String serve() {
		
		return restService.recent20ToJson();
	}
	
}

package de.fim.unipassau.WDE.EventManagementSystem.Services;

import de.fim.unipassau.WDE.EventManagementSystem.Model.Event;

/**
 * A simple declaration of methods expected from an implementing class.
 * @author Jonas Picker
 *
 */
public interface EventService {
	
	/**
	 * This should transform the data held by an Event Object into a JSON.
	 * 
	 * @return a String in JSON format.
	 */
	String toJson();
	
	/**
	 * This should check if an Event lies in the past of the moment this method is invoked.
	 * 
	 * @return a boolean representing the condition above.
	 */
	boolean isOutDated();

	/**
	 * Produces a new Event from the parameters passed to this method in JSON Format.
	 * 
	 * @param json the text String containing the events field values.
	 * @return A new database Entry.
	 */
	Event fromJson(String json);

	/**
	 * Accesses the Likes/Dislikes Fields in order to update the Database.
	 * 
	 * @param ususally like adding or subtracting one
	 * @param dislike ususally like adding or subtracting one
	 */
	void updateLikesAndDislikes(int likes, int dislikes);
	
}

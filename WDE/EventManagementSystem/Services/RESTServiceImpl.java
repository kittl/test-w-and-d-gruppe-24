package de.fim.unipassau.WDE.EventManagementSystem.Services;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Service;

import de.fim.unipassau.WDE.EventManagementSystem.DAO.EventRepository;
import de.fim.unipassau.WDE.EventManagementSystem.Model.Event;

/**
 * This class capsules functionality used to fill the response body for the REST end-point.
 * @author Jonas Picker
 *
 */
@Service
public class RESTServiceImpl implements RESTService {
	
	/**
	 * The Database access interface used.
	 */
	private final EventRepository eventRepository;
	
	/**
	 * A normal constructor.
	 * 
	 * @param repo initializes the field.
	 */
	public RESTServiceImpl(EventRepository repo) {
		this.eventRepository = repo;
	}

	/**
	 * Converts the query result.
	 * 
	 * @return A String representing a JSON Format.
	 */
	@Override
	public String recent20ToJson() {
		LocalDateTime now = LocalDateTime.now();
		List<Event> last20 = eventRepository.findTop20ByOrderByDateDesc(now);
		StringBuilder jsonString = new StringBuilder("{");
		
		for(int i = 1; i <= 20; i++) {
			EventServiceImpl eventService = new EventServiceImpl(last20.get(i - 1));
			jsonString.append("\"Event\"" + i + " : " + eventService.toJson() + ",");
		}
		
		jsonString.append("}");
		
		
		return jsonString.toString();
	}
	
	
}

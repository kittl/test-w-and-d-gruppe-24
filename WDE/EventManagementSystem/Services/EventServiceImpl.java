package de.fim.unipassau.WDE.EventManagementSystem.Services;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.json.JSONObject;
import org.springframework.stereotype.Service;

import de.fim.unipassau.WDE.EventManagementSystem.Model.Event;
import de.fim.unipassau.WDE.EventManagementSystem.Model.Location;


/**
 * This provides additional functionality for Events.
 * @author Jonas Picker
 *
 */
@Service
public class EventServiceImpl implements EventService {
	
	/**
	 * The Event to be serviced.
	 */
	private Event event;
	
	/**
	 * A normal constructor
	 * 
	 * @param event To initialize the field.
	 */
	public EventServiceImpl(Event event) {
		this.event = event;
	}
	
	/**
	 * Takes all fields of the Event of this Service and stuffs them into a JSON.
	 * 
	 * @return A String in JSON format.
	 */
	@Override
	public String toJson() {
		String json = "{ "
				+ "\"Name\" : " + "\"" + event.getName() + "\""  + ","
				+ "\"Location\" : " + "\"" + event.getLocation() + "\""  + ","
				+ "\"Date\" : " + "\"" + event.getDate().toString() + "\""  + ","
				+ "\"Type\" : " + "\"" + event.getType() + "\"" + ","
				+ "\"Description\" : " + "\"" + event.getDescription() + "\""  + ","
				+ "\"Likes\\\" : " + "\"" + event.getLikes() + "\"" + ","
				+ "\"Dislikes\" : " + "\"" + event.getDislikes() + "\"" 
				+ " }";
		return json;
	}
	
	@Override
	public Event fromJson(String textJson) {
		JSONObject json = new JSONObject(textJson);
		
		LocalDateTime datetime = LocalDateTime.parse(json.getString("date"));
		
		Location location = new Location(json.getString("location"));
		
		if (json.getBoolean("isGeoPos")) {
			String[] lattAndLong = json.getString("location").split(",");
			int latt = Integer.parseInt(lattAndLong[1]);
			int longi = Integer.parseInt(lattAndLong[2]);
			location = new Location(latt, longi);
		}
		
		return new Event(json.getString("name"), location, datetime, json.getString("type"), json.getString("description"), json.getInt("likes"), json.getInt("dislikes"));
	}
	
	/**
	 * Checks if this Event lies in the past already.
	 * 
	 * @return true if this is the case.
	 */
	@Override
	public boolean isOutDated() {
		LocalDate now = LocalDate.now();
		LocalDate begin = event.getDate().toLocalDate();
		return begin.isBefore(now);
	}
	
	/**
	 * Accesses the Likes/Dislikes Fields in order to update the Database.
	 * 
	 * @param ususally like adding or subtracting one
	 * @param dislike ususally like adding or subtracting one
	 */
	@Override
	public void updateLikesAndDislikes(int likes, int dislikes) {
		this.event.setLikes(this.event.getLikes() + likes);
		this.event.setDislikes(this.event.getDislikes() + dislikes);
	}
}

package de.fim.unipassau.WDE.EventManagementSystem.Services;

/**
 * A simple declaration of methods necessary for the REST end-point to function.
 * @author Jonas Picker
 *
 */
public interface RESTService {
	
	/**
	 * A representaion of data from the recent 20 Events.
	 * 
	 * @return A String containing the information in JSON format.
	 */
	String recent20ToJson();

}
